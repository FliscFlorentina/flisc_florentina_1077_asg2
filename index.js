
const FIRST_NAME = "Florentina";
const LAST_NAME = "Flisc";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function initCaching() {

    var cache=new Object();
    cache.getCache=function(){
        return cache;
    }
    cache.pageAccessCounter=function(section='home')
    {
        section=new String(section).toLowerCase();
        if(cache.hasOwnProperty(section)){
            this[section]++;
        }
        else{
            Object.defineProperty(cache,section,{
                value:1,
                writable:true
            });
        }
    }
   
    return cache;
   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

